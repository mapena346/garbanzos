import React, {Component} from 'react';

class Counter extends Component {
    constructor(props)
    {
        super(); // Tiene que llamar a la clase padre
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            contador: 0
        }
    }
    incrementar()
    {
        this.setState({
            contador: this.state.contador + 1
          })
    }

    handleClick(e)
    {
        alert('Click!');
    }
    
    render (){
        return (
            <div>
                <button onClick={this.incrementar.bind(this)}>Click me</button>
                <p>{
                    <font size={this.state.contador}>{'Garbanzos!!! ' + this.state.contador}</font>
                    }</p>
                
            </div>
        );
    }
}
export default Counter;

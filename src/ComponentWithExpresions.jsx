import React, {Component} from 'react';
class ComponentWithExpressions extends Component
{
    render()
    {
        let user={
            name: 'Miguel Angel',
            surname: 'Penya'
        };
        return (
            <div> 
                <p> El nombre es: {user.name}</p> 
                <p> El apellido es: {user.surname}</p> 
            </div>
        );
    };   
};
export default ComponentWithExpressions;
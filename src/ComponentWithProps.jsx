import React, {Component} from 'react';

class ComponentWithProps extends Component 
{
        render()
        {
            const {text, number,thing,func}=this.props;
            return ( <div>
                <p>Texto: {text}</p>
                <p>Numero: {number}</p>
                <p>Objecto: {thing.name}{thing.surname}</p>
                <p>{func()}</p>
            </div>);
        };
};
export default ComponentWithProps;
/*
<ComponentWithProps 
    text="Example with text"
    number={6}
    thing={obj}
    func={myFunction}
    />
 */
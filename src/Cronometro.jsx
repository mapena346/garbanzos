import React, {Component} from 'react';
import CronometroHeader from './CronometroHeader';
import CronometroContent from './CronometroContent';
import CronometroButtons from "./CronometroButtons";
import extractTimeParts from './utils';

export default class Cronometro extends Component {
    constructor(){
        super();
        this.handleStart = this.handleStart.bind(this);
        this.handleStop = this.handleStop.bind(this);        
        this.state = {
			estaFuncionando: false,
			inicio: 0,
			actual: 0
		}
    }    
    
    handleStart(event){
        //Pulsar el boton start        
        console.log('Start Buttons');        
        if(this.state.estaFuncionando===false)
        {
            //el Crono está parado y hay que ponerlo en marcha
            this.setState({
				estaFuncionando: true,
				inicio: Date.now(),
				actual: Date.now()
			})

			this._interval = setInterval(() =>{
				this.setState({
					actual: Date.now()
				})
			}, 50)
        }
    }

    handleStop(event){
        console.log('Stop Buttons');        
        if ( this.state.estaFuncionando ) {
			// parara el crono si esta funcionando.
			clearInterval(this._interval)
			this.setState({
				estaFuncionando: false
			})
		} else {
			// reseteara el crono
			this.setState({
				inicio: 0,
				actual: 0
			})
		}
    }

    render()
    {
        const { inicio, actual } = this.state,
			{
				hours,
				minutes,
				seconds,
            } = extractTimeParts( actual - inicio )
            
        return(
            <div className="crono">
                <CronometroHeader />
                <div className="content">
                <CronometroContent
                horas = { hours }
                minutos = { minutes }
                segundos = { seconds }
                />  
                <CronometroButtons onStart={this.handleStart} onStop={this.handleStop} />  
                </div>                                           
            </div>
        );
    }
}



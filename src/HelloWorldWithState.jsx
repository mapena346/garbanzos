import React, {Component} from 'react';
import PropTypes from 'prop-types';

class HelloWorldWithState extends Component{
    constructor (props)
    {
        super(props);
        this.state=
       {
           count: props.initialCount
       };
    }
    render()
    {
        return (
            <div className="panel"> 
                <h1>
                    Hola mundo! Este es mi estado: {this.state.count}
                </h1>
             </div>
        );
    }
};

HelloWorldWithState.propTypes =
{
    initialCount: PropTypes.number
};

HelloWorldWithState.defaultProps=
{
    initialCount: 2
};
export default HelloWorldWithState;
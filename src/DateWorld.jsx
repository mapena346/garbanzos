import React, {Component} from 'react';
import DateItem from './DateItem';

const timeZone= [
    { country: 'España', difUTC: 0 },
    { country: 'Inglaterra', difUTC: -1},
    { country: 'Venezuela', difUTC: -6}];

const moveDate= (date, difUTC) =>{
    var dateMoved = new Date(date);
    dateMoved.setUTCHours(dateMoved.getUTCHours()+difUTC);
    return dateMoved;
};

class DateWorld extends Component{    
    render(){
        let now=new Date(),
            dates=timeZone.map(Zone=>
                <DateItem
                 key={Zone.country}
                 country={ Zone.country}
                 date= { moveDate(now, Zone.difUTC)}
                />);

        return(
            <div><h1>Fechas del mundo</h1>{ dates }</div>
        );
    };
};

export default DateWorld;
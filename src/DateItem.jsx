import React, {Component} from 'react';
import PropTypes from 'prop-types'; // Se debe incluir para hacer checkeos sobre el tipo de variables que son invocadas.

class DateItem extends Component{
    render(){
        const { country,date } = this.props;
        return (
            <p> En {country} son las {date.toTimeString()}.</p>
        );
    };
};
//Metera warnings si hay algun dato que no sea del type indicado.
DateItem.propTypes ={
    country: PropTypes.string.isRequired,
    date: PropTypes.object.isRequired
}
//Fija valores por defecto si no se le pasan valores.
DateItem.defaultProps ={
    country: 'España',
    date: new Date()
}

export default DateItem; // Se puede poner directamente en la funcion export default class date item extends Component (en la cabecera)
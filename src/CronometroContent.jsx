import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class CronometroContent extends Component{
    render(){
        return(        
            <div className="timer">
                <span className="timer-hours">{this.props.horas}</span>:
                <span className="timer-minutes">{this.props.minutos}</span>:
                <span className="timer-seconds">{this.props.segundos}</span>
            </div>
          
        );
    }
}
CronometroContent.shape={
    horas: PropTypes.string,
    minutos: PropTypes.string,
    seconds: PropTypes.string
};
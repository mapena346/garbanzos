import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class CronometroButtons extends Component {
    render() {
        return (
        <div className="actions">
            <button onClick={this.props.onStop}>STOP</button>
            <button onClick={this.props.onStart}>START</button>
        </div>
        );
    }
}

CronometroButtons.propTypes={
    onStop: PropTypes.func,
    onStart: PropTypes.func
};


import React, {Component} from 'react';
import Buttons from './Buttons.jsx';

class Parent extends Component {
    constructor(){
        super();
        this.handleStart = this.handleStart.bind(this);
        this.handleStop = this.handleStop.bind(this);        
    }    
    
    handleStart(event){
        console.log('Start Buttons');        
    }

    handleStop(event){
        console.log('Stop Buttons');        
    }

    render()
    {
        return(
            <div>
                <Buttons onStart={this.handleStart} onStop={this.handleStop} />
            </div>
        );
    };
}
export default Parent;
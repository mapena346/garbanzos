import React, {Component, Fragment} from 'react';
import HelloWorld from './HelloWordES2015';
import HelloWorldEWithStyle from './HelloWorldEWithStyle';
import ComponentWithExpressions from './ComponentWithExpresions';
import ListComponent from './ListComponent';

class ConFragmentos extends Component
{
    render()
    {
        return (
            <Fragment>
                <HelloWorld />
                <HelloWorldEWithStyle />
                <ComponentWithExpressions />
                <ListComponent />
            </Fragment>
        );
    };   
};
export default ConFragmentos;
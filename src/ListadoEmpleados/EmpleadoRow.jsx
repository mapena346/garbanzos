import React,{Component} from 'react';
import EmpleadoAvatar from './EmpleadoAvatar';
import PropTypes from 'prop-types'; // Se debe incluir para hacer checkeos sobre el tipo de variables que son invocadas.
//import al empleadoIMAG

export default class EmpleadoRow extends Component {
    render()
    {
        const { empleado: {id, name: nombre, surname: apellido} }  = this.props;
        return(
            //aqui agregamos las filas de empleados.            
            <div className="FilaEmpleados">
               <li> <EmpleadoAvatar /><span>{id} {nombre} {apellido}</span> </li>
            </div>
        );

    }

}

EmpleadoRow.propTypes ={
    empleado: PropTypes.object.isRequired,
}


import React,{Component} from 'react';
import EmpleadoRow from './EmpleadoRow';
import empleados from './empleados';


export default class EmpleadoList extends Component {


    render()
    {
        //debugger;

        return(<div>
              <h1>Listado de Empleados</h1>
              <ul>
              { empleados.map(empleado => <EmpleadoRow key = {empleado.id} empleado={ empleado } />) }
              </ul>
          </div>
        );
    }
};
import React, {Component} from 'react';
import PropTypes from 'prop-types';
class Buttons extends Component
{
    render()
    {
        return(
        <div className="actions">
            <button onClick={this.props.onStop}> stop </button>
            <button onClick={this.props.onStart}> start </button>
        </div>
        );
    }
};

Buttons.propTypes={
    onStop: PropTypes.func,
    onStart: PropTypes.func
};
export default Buttons;